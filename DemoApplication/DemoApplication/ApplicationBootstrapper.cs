﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Ninject;
using Ninject.Modules;

namespace DemoApplication
{
    public class ApplicationBootstrapper
    {
        /// <summary>
        /// Application kernel.
        /// </summary>
        private IKernel _kernel = null;
        
        /// <summary>
        /// Custom Kernel builder function for this application.
        /// </summary>
        /// <returns>The kernel.</returns>
        private static IKernel BuildKernel()
        {
            List<INinjectModule> modules = new List<INinjectModule>
            {
                DemoApplication.IoC.ModuleBuilder.GetModule(),
            };
            return new StandardKernel(modules.ToArray());
        }

        /// <summary>
        /// Starts the application exectuion.
        /// </summary>
        public void Run()
        {
            _kernel = BuildKernel();
            Globals.Kernel = _kernel;

            OpenUserInterface();
        }

        /// <summary>
        /// Opens the User Interface.
        /// </summary>
        private void OpenUserInterface()
        {
            Application.Current.MainWindow = _kernel.Get<MainWindow>();
            Application.Current.MainWindow.Show();
        }
        
    }
}
