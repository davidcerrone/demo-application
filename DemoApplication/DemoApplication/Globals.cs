﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;

namespace DemoApplication
{
    /// <summary>
    /// Global variables for the application.
    /// </summary>
    public static class Globals
    {
        /// <summary>
        /// Ninject kernel.
        /// </summary>
        public static IKernel Kernel { get; set; }
    }
}
