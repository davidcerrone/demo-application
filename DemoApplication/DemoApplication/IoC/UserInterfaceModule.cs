﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DemoApplication.ViewModels;
using DemoApplication.ViewModels.Interfaces;
using Ninject.Modules;

namespace DemoApplication.IoC
{
    /// <summary>
    /// Ninject module for binding UI Contexts.
    /// </summary>
    public class UserInterfaceModule : NinjectModule
    {
        /// <summary>
        /// Loads the Ninject module type bindings all UI contexts.
        /// </summary>
        public override void Load()
        {
            // Bind the view models.
            Bind<ITimeViewModel>().To<TimeViewModel>();
            Bind<IDemoButtonViewModel>().To<DemoButtonViewModel>();
        }
    }
}
