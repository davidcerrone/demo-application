﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Text;
using System.Threading.Tasks;
using DemoApplication.ViewModels.Interfaces;
using Reactive.Bindings;

namespace DemoApplication.ViewModels
{
    internal class DemoButtonViewModel : IDemoButtonViewModel
    {
        /// <summary>
        /// Button command
        /// </summary>
        public ReactiveCommand ButtonCommand { get; private set; }

        /// <summary>
        /// Demo text to update.
        /// </summary>
        public ReactiveProperty<string> DemoText { get; private set; }

        /// <summary>
        /// Disposables.
        /// </summary>
        private readonly CompositeDisposable _disposables;

        /// <summary>
        /// Current count.
        /// </summary>
        private int _count = 0;

        /// <summary>
        /// String to append to DemoText.
        /// </summary>
        private const string TextString = "Count: ";

        /// <summary>
        /// Constructor.
        /// </summary>
        public DemoButtonViewModel(CompositeDisposable disposables)
        {
            _disposables = disposables;

            CreateProperties();
            CreateCommands();
            Subscribe();
        }

        /// <summary>
        /// Creates the reactive properties.
        /// </summary>
        private void CreateProperties()
        {
            _disposables.Add(DemoText = new ReactiveProperty<string>(TextString + _count));
        }

        /// <summary>
        /// Creates the reactive commands.
        /// </summary>
        private void CreateCommands()
        {
            _disposables.Add(ButtonCommand = new ReactiveCommand());
        }

        /// <summary>
        /// Subscribe to events.
        /// </summary>
        private void Subscribe()
        {
            _disposables.Add(ButtonCommand.Subscribe(_ => ButtonCommandExecute()));
        }

        /// <summary>
        /// Execute the button press.
        /// </summary>
        private void ButtonCommandExecute()
        {
            _count++;
            DemoText.Value = TextString + _count;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _disposables.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
