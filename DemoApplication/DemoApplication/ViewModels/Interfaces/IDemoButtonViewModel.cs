﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Reactive.Bindings;

namespace DemoApplication.ViewModels.Interfaces
{
    public interface IDemoButtonViewModel : IDisposable
    {
        /// <summary>
        /// Button command
        /// </summary>
        ReactiveCommand ButtonCommand { get; }
        
        /// <summary>
        /// Demo text to update.
        /// </summary>
        ReactiveProperty<string> DemoText { get; }
    }
}
