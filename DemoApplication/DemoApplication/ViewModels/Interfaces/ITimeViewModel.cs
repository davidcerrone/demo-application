﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Reactive.Bindings;

namespace DemoApplication.ViewModels.Interfaces
{
    public interface ITimeViewModel : IDisposable
    {
        /// <summary>
        /// Date string.
        /// </summary>
        ReactiveProperty<string> Date { get; }

        /// <summary>
        /// Time string.
        /// </summary>
        ReactiveProperty<string> Time { get; }
    }
}
