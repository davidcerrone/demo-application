﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using DemoApplication.ViewModels.Interfaces;
using Reactive.Bindings;

namespace DemoApplication.ViewModels
{
    internal class TimeViewModel : ITimeViewModel
    {
        /// <summary>
        /// Disposables.
        /// </summary>
        private readonly CompositeDisposable _disposables;

        /// <summary>
        /// Date string.
        /// </summary>
        public ReactiveProperty<string> Date { get; private set; }

        /// <summary>
        /// Time string.
        /// </summary>
        public ReactiveProperty<string> Time { get; private set; }

        /// <summary>
        /// Time update interval
        /// </summary>
        private const long DateTimeUpdateIntervalInMilliseconds = 500;

        /// <summary>
        /// Timer to update time.
        /// </summary>
        private IObservable<long> _timer;

        /// <summary>
        /// Constructor.
        /// </summary>
        public TimeViewModel(CompositeDisposable disposables)
        {
            _disposables = disposables;

            CreateProperties();
        }

        /// <summary>
        /// Creates the properties.
        /// </summary>
        private void CreateProperties()
        {
            _timer = Observable.Interval(TimeSpan.FromMilliseconds(DateTimeUpdateIntervalInMilliseconds));

            Date = _timer.Select(_ => $"{DateTime.Now:MM/dd/yyyy}").ToReactiveProperty();
            Time = _timer.Select(_ => $"{DateTime.Now:hh:mm tt}".ToLower()).ToReactiveProperty();

            _disposables.Add(Date);
            _disposables.Add(Time);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _disposables.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
